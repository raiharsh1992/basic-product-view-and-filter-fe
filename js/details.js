$(document).ready(function() {
  if (isDirect == 1) {
    $("#inserNewCommentSection").hide();
  }
  displayProducts();
});


function displayProducts() {
  var myObj = {};
  myObj["movieId"] = getUrlVars()["movieId"];
  var jsonObj = JSON.stringify(myObj);
  var url = beUrl() + 'moviedetails/';
  $.ajax({
    url: url,
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    success: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      var usingData = usingInfo.Data;
      document.getElementById("movieName").innerHTML = usingData.name;
      $("#director").append(usingData.director);
      $("#popularity").append(usingData.popularity);
      $("#imdb").append(usingData.imdb);
      var lPrint = ''
      $.each(usingData.genre, function(index, value) {
        if (index == 0) {
          lPrint = lPrint + value;
        } else {
          lPrint = lPrint + ", " + value;
        }
      })
      $("#genre").append(lPrint);
      lPrint = '';
      $.each(usingData.comments, function(index, value) {
        lPrint = lPrint + "<tr class='hoverable'><td>" + value.commentBy + "</td><td>" + value.comment + "</td></tr>"
      })
      $("#tableContainer").append(lPrint);
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });
}

function insertNewComment() {
  event.preventDefault();
  if (document.getElementById("textarea1").value != "") {
    var myObj = {};
    myObj['userId'] = sessionInfo.sessionInfo.userId;
    myObj['typeId'] = sessionInfo.sessionInfo.typeId;
    myObj['userType'] = sessionInfo.sessionInfo.userType;
    myObj['movieId'] = getUrlVars()["movieId"];
    myObj['comment'] = document.getElementById("textarea1").value;
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
      url: beUrl() + "commentmovie/",
      type: 'POST',
      dataType: 'json',
      processData: false,
      contentType: 'application/json',
      data: jsonObj,
      headers: {
        'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
        'sessionKey': sessionInfo.sessionInfo.sessionKey
      },
      success: function(response) {
        M.toast({
          html: 'Comment created successfully'
        });
        location.reload();
      },
      error: function(response) {
        var usingInfo = JSON.parse(JSON.stringify(response));
        M.toast({
          html: usingInfo.responseJSON.Data
        });
        document.getElementById("displayLoading").style.display = "none";
        if (usingInfo.status == 401) {
          logoutModule();
        }
      }
    });
  } else {
    M.toast({
      html: "Comment section cannot be left blank"
    });
  }
}

function getUrlVars() {

  var vars = {};
  var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
    vars[key] = value;

  });
  return vars;
}
