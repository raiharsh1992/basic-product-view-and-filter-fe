var pageSize=100, pageCount=1, defaultViewType=0, filterValue;
$(document).ready(function() {
  if(isDirect==0){
    console.log(sessionInfo);
  }
  displayProducts();
  displayFilterValues()
});

$(document).on("change", '#filterInformation', function (event) {
  filterValue = $(this).children(":selected").attr("value");
  console.log(filterValue);
  pageCount = 1;
  defaultViewType = 1;
  displayFilterProducts(pageSize,  pageCount)
});

function searchItem(){
  event.preventDefault();
  if(document.getElementById("icon_prefix").value!=""){
    pageCount = 1;
    defaultViewType = 2;
    filterValue = document.getElementById("icon_prefix").value;
    displaySearchProducts();
  }
}

function displayFilterValues(){
  var myObj = {};
  myObj["viewType"]="CLIENT";
  var jsonObj = JSON.stringify(myObj);
  var url = beUrl() + 'viewgenre/';
  $.ajax({
    url: url,
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    success: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      var usingData = usingInfo.Data;

      if (usingData.length > 0) {
        var lPrint = '';
        $.each(usingData, function(index, v) {
          lPrint = lPrint+"<option value="+v.genreName+">"+v.genreName+"</option>";
        });
        $("#filterInformation").append(lPrint);
        $('#filterInformation').formSelect();
      }
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      console.log(usingInfo);
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });
}

function displayFilterProducts(){
  var myObj = {};
  myObj["totalViewCount"]=pageSize;
  myObj["pageCount"]=pageCount;
  myObj["searchValue"] = filterValue;
  var jsonObj = JSON.stringify(myObj);
  var url = beUrl() + 'viewfilters/';
  $.ajax({
    url: url,
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    success: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      var usingData = usingInfo.Data;

      if (usingData.length > 0) {
        var lPrint = '';
        $.each(usingData, function(index, v) {
          lPrint = lPrint+"<tr class='hoverable'><td class='inLineText'>" + v.name + "</td><td>" + v.director + "</td><td>" + v.popularity + "</td><td>" + v.imdb + "</td><td><a href='javascript:goToDetails("+v.movieId+")'>Details</a></td></tr>";
        });

        document.getElementById("displayNothing").style.display = "none";
        document.getElementById("tableContainer").innerHTML = lPrint;
        var currentDisplayBuffer = parseInt(pageSize)*parseInt(pageCount);
        if(usingInfo.totalMovieCount>currentDisplayBuffer){
          document.getElementById("nextButton").style.display = "inline-block";
        }
        else{
          document.getElementById("nextButton").style.display = "none";
        }
        if(parseInt(pageCount)==1){
          document.getElementById("prevButton").style.display = "none";
        }
        else{
          document.getElementById("prevButton").style.display = "inline-block";
        }
      } else {
        M.toast({
          html: 'No products to display please try again later!'
        });
        document.getElementById("tableContainer").innerHTML = "";
        document.getElementById("deleteButton").style.display = "none";
        document.getElementById("prevButton").style.display = "none";
        document.getElementById("nextButton").style.display = "none";
      }
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      console.log(usingInfo);
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });
}

function displaySearchProducts(){
  var myObj = {};
  myObj["totalViewCount"]=pageSize;
  myObj["pageCount"]=pageCount;
  myObj["searchValue"] = filterValue;
  var jsonObj = JSON.stringify(myObj);
  var url = beUrl() + 'search/';
  $.ajax({
    url: url,
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    success: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      var usingData = usingInfo.Data;

      if (usingData.length > 0) {
        var lPrint = '';
        $.each(usingData, function(index, v) {
          lPrint = lPrint+"<tr class='hoverable'><td class='inLineText'>" + v.name + "</td><td>" + v.director + "</td><td>" + v.popularity + "</td><td>" + v.imdb + "</td><td><a href='javascript:goToDetails("+v.movieId+")'>Details</a></td></tr>";
        });

        document.getElementById("displayNothing").style.display = "none";
        document.getElementById("tableContainer").innerHTML = lPrint;
        var currentDisplayBuffer = parseInt(pageSize)*parseInt(pageCount);
        if(usingInfo.totalMovieCount>currentDisplayBuffer){
          document.getElementById("nextButton").style.display = "inline-block";
        }
        else{
          document.getElementById("nextButton").style.display = "none";
        }
        if(parseInt(pageCount)==1){
          document.getElementById("prevButton").style.display = "none";
        }
        else{
          document.getElementById("prevButton").style.display = "inline-block";
        }
      } else {
        M.toast({
          html: 'No products to display please try again later!'
        });
        document.getElementById("tableContainer").innerHTML = "";
        document.getElementById("deleteButton").style.display = "none";
        document.getElementById("prevButton").style.display = "none";
        document.getElementById("nextButton").style.display = "none";
      }
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      console.log(usingInfo);
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });
}

function displayProducts(){
  var myObj = {};
  myObj["totalCount"]=pageSize;
  myObj["pageCount"]=pageCount;
  var jsonObj = JSON.stringify(myObj);
  var url = beUrl() + 'viewmovie/';
  $.ajax({
    url: url,
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    success: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      var usingData = usingInfo.Data;

      if (usingData.length > 0) {
        var lPrint = '';
        $.each(usingData, function(index, v) {
          lPrint = lPrint+"<tr class='hoverable'><td class='inLineText'>" + v.name + "</td><td>" + v.director + "</td><td>" + v.popularity + "</td><td>" + v.imdb + "</td><td><a href='javascript:goToDetails("+v.movieId+")'>Details</a></td></tr>";
        });

        document.getElementById("displayNothing").style.display = "none";
        document.getElementById("tableContainer").innerHTML = lPrint;
        var currentDisplayBuffer = parseInt(pageSize)*parseInt(pageCount);
        if(usingInfo.totalMovieCount>currentDisplayBuffer){
          document.getElementById("nextButton").style.display = "inline-block";
        }
        else{
          document.getElementById("nextButton").style.display = "none";
        }
        if(parseInt(pageCount)==1){
          document.getElementById("prevButton").style.display = "none";
        }
        else{
          document.getElementById("prevButton").style.display = "inline-block";
        }
      } else {
        M.toast({
          html: 'No products to display please try again later!'
        });
        document.getElementById("tableContainer").innerHTML = "";
        document.getElementById("deleteButton").style.display = "none";
        document.getElementById("prevButton").style.display = "none";
        document.getElementById("nextButton").style.display = "none";
      }
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      console.log(usingInfo);
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });
}

function goToNext() {
  if(defaultViewType==0){
    pageCount=pageCount+1
    displayProducts()
  }
  else if(defaultViewType==1){
    pageCount=pageCount+1
    displayFilterProducts()
  }
  else if(defaultViewType==2){
    pageCount=pageCount+1
    displaySearchProducts()
  }
}

function goToPrevious() {
  if(defaultViewType==0){
    if(pageCount>1){
      pageCount=pageCount-1
      displayProducts()
    }
  }
  else if(defaultViewType==1){
    if(pageCount>1){
      pageCount=pageCount-1
      displayFilterProducts()
    }
  }
  else if(defaultViewType==2){
    if(pageCount>1){
      pageCount=pageCount-1
      displaySearchProducts()
    }
  }
}

function goToDetails(movieId) {
  location.assign("details.html?movieId="+movieId)
}
