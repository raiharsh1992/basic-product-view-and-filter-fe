var sessionInfo;
var isDirect = 0;
var loginUser = '<li class="bold"><a href="login.html" class="waves-effect waves-teal inLineText"><img src="img/login.png" class="menuManagerIcon" /><span class="fontSize">Login</span></a></li>';
var moviesVol = '<li class="bold"><a href="movies.html" class="waves-effect waves-teal inLineText"><img src="img/movie.png" class="menuManagerIcon" /><span class="fontSize">Movies</span></a></li>';
var logOut = '<li class="bold"><a href="javascript:logoutModule()" class="waves-effect waves-teal inLineText"><img src="img/logout.png" class="menuManagerIcon" /><span class="fontSize">Logout</span></a></li>';

$(document).ready(function() {
  if (localStorage.getItem("rememberMe")) {
    var rememberMe = parseInt(localStorage.getItem("rememberMe"));
    if (rememberMe == 1) {
      if (localStorage.getItem("clientSessionInfo")) {
        sessionInfo = JSON.parse(localStorage.getItem("clientSessionInfo"));
      } else {
        if (!sessionStorage.getItem("skippedItem")){
          location.replace("login.html");
        }
        else{
          isDirect=1;
        }
      }
    } else {
      if (sessionStorage.getItem("clientSessionInfo")) {
        sessionInfo = JSON.parse(sessionStorage.getItem("clientSessionInfo"));
      } else {
        if (!sessionStorage.getItem("skippedItem")){
          location.replace("login.html");
        }
        else{
          isDirect=1;
        }
      }
    }
  } else {
    if (!sessionStorage.getItem("skippedItem")){
      location.replace("login.html");
    }
    else{
      isDirect=1;
    }
  }
  menuManager();

});

function menuManager() {
  var menuPrint = '';
  menuPrint = menuPrint + moviesVol;
  if(isDirect==1){
    menuPrint = menuPrint + loginUser;
  }
  else{
    menuPrint = menuPrint + logOut;
  }
  $("#nav-mobile").append(menuPrint);
}

function logoutModule() {
  var myObj = {};
  myObj['userId'] = sessionInfo.sessionInfo.userId;
  myObj['typeId'] = sessionInfo.sessionInfo.typeId;
  myObj['userType'] = sessionInfo.sessionInfo.userType;
  var jsonObj = JSON.stringify(myObj);
  $.ajax({
    url: beUrl() + "logout/",
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
      'sessionKey':sessionInfo.sessionInfo.sessionKey
    },
    success: function(response) {
      M.toast({
        html: 'User logged out successfully'
      });
      localStorage.removeItem("clientSessionInfo");
      sessionStorage.removeItem("clientSessionInfo");
      localStorage.removeItem("rememberMe");
      location.replace("login.html");
    },
    error: function(response) {
      M.toast({
        html: 'Issue while logging out user'
      });
      localStorage.removeItem("clientSessionInfo");
      sessionStorage.removeItem("clientSessionInfo");
      localStorage.removeItem("rememberMe");
      location.replace("login.html");
    }
  });

}
