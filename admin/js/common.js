var sessionInfo;
var isDirect = 0;
var loginUser = '<li class="bold"><a href="createuser.html" class="waves-effect waves-teal inLineText"><img src="img/login.png" class="menuManagerIcon" /><span class="fontSize">Create User</span></a></li>';
var moviesVol = '<li class="bold"><a href="movies.html" class="waves-effect waves-teal inLineText"><img src="img/movie.png" class="menuManagerIcon" /><span class="fontSize">Movies</span></a></li>';
var viewGenre = '<li class="bold"><a href="genre.html" class="waves-effect waves-teal inLineText"><img src="img/view.png" class="menuManagerIcon" /><span class="fontSize">View Genre</span></a></li>';
var createGenre = '<li class="bold"><a href="creategenre.html" class="waves-effect waves-teal inLineText"><img src="img/create.png" class="menuManagerIcon" /><span class="fontSize">Create Genre</span></a></li>';
var createMovie = '<li class="bold"><a href="createmovie.html" class="waves-effect waves-teal inLineText"><img src="img/product.png" class="menuManagerIcon" /><span class="fontSize">Create Movies</span></a></li>';
var bulkMovie = '<li class="bold"><a href="bulkupload.html" class="waves-effect waves-teal inLineText"><img src="img/upload.png" class="menuManagerIcon" /><span class="fontSize">Bulk Movies Upload</span></a></li>';
var logOut = '<li class="bold"><a href="javascript:logoutModule()" class="waves-effect waves-teal inLineText"><img src="img/logout.png" class="menuManagerIcon" /><span class="fontSize">Logout</span></a></li>';

$(document).ready(function() {
  if (localStorage.getItem("adminRememberMe")) {
    var rememberMe = parseInt(localStorage.getItem("adminRememberMe"));
    if (rememberMe == 1) {
      if (localStorage.getItem("adminSessionInfo")) {
        sessionInfo = JSON.parse(localStorage.getItem("adminSessionInfo"));
      } else {
        location.replace("login.html");
      }
    } else {
      if (sessionStorage.getItem("adminSessionInfo")) {
        sessionInfo = JSON.parse(sessionStorage.getItem("adminSessionInfo"));
      } else {
        location.replace("login.html");
      }
    }
  } else {
    location.replace("login.html");
  }
  menuManager();

});

function menuManager() {
  var menuPrint = '';
  menuPrint = menuPrint + moviesVol + createMovie + bulkMovie + viewGenre + createGenre;
  menuPrint = menuPrint + loginUser;
  menuPrint = menuPrint + logOut;
  $("#nav-mobile").append(menuPrint);
}

function logoutModule() {
  var myObj = {};
  myObj['userId'] = sessionInfo.sessionInfo.userId;
  myObj['typeId'] = sessionInfo.sessionInfo.typeId;
  myObj['userType'] = sessionInfo.sessionInfo.userType;
  var jsonObj = JSON.stringify(myObj);
  $.ajax({
    url: beUrl() + "logout/",
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
      'sessionKey':sessionInfo.sessionInfo.sessionKey
    },
    success: function(response) {
      M.toast({
        html: 'User logged out successfully'
      });
      localStorage.removeItem("adminSessionInfo");
      sessionStorage.removeItem("adminSessionInfo");
      localStorage.removeItem("adminRememberMe");
      location.replace("login.html");
    },
    error: function(response) {
      M.toast({
        html: 'Issue while logging out user'
      });
      localStorage.removeItem("adminSessionInfo");
      sessionStorage.removeItem("adminSessionInfo");
      localStorage.removeItem("adminRememberMe");
      location.replace("login.html");
    }
  });

}
