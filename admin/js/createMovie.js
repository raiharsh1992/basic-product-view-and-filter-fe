function createNewMovie() {
  event.preventDefault();
  var movieName = document.getElementById("movieName").value;
  var director = document.getElementById("director").value;
  var poplularity = document.getElementById("poplularity").value;
  var imdb = document.getElementById("imdb").value;
  var genre = document.getElementById("genre").value;
  if (movieName != "") {
    if (director != "") {
      if (poplularity != "") {
        if (imdb != "") {
          if (genre != "") {
            var myObj = {};
            myObj["name"] = movieName;
            myObj["director"] = director;
            myObj["poplularity"] = parseFloat(poplularity);
            myObj["imdb"] = parseFloat(imdb);
            myObj["genre"] = genre.split(",");
            myObj['userId'] = sessionInfo.sessionInfo.userId;
            myObj['typeId'] = sessionInfo.sessionInfo.typeId;
            myObj['userType'] = sessionInfo.sessionInfo.userType;
            var jsonObj = JSON.stringify(myObj);
            $.ajax({
              url: beUrl() + 'createmovie/',
              type: 'POST',
              dataType: 'json',
              processData: false,
              contentType: 'application/json',
              data: jsonObj,
              headers: {
                'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
                'sessionKey': sessionInfo.sessionInfo.sessionKey
              },
              success: function(response) {
                M.toast({
                  html: 'New movie created'
                });
                location.assign("movies.html");
              },
              error: function(response) {
                var usingInfo = JSON.parse(JSON.stringify(response));
                M.toast({
                  html: usingInfo.responseJSON.Data
                });
              }
            });
          } else {
            M.toast({
              html: 'Genre cannot be left blank'
            });
          }
        } else {
          M.toast({
            html: 'IMDB cannot be left blank'
          });
        }
      } else {
        M.toast({
          html: 'Popularity cannot be left blank'
        });
      }
    } else {
      M.toast({
        html: 'Director cannot be left blank'
      });
    }
  } else {
    M.toast({
      html: 'Movie name cannot be left blank'
    });
  }
}
