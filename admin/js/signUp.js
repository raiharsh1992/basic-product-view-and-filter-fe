var sessionInfo;
$(document).ready(function() {
  if (localStorage.getItem("adminRememberMe")) {
    var rememberMe = parseInt(localStorage.getItem("adminRememberMe"));
    if (rememberMe == 1) {
      if (localStorage.getItem("adminSessionInfo")) {
        sessionInfo = JSON.parse(localStorage.getItem("adminSessionInfo"));
      } else {
        location.replace("login.html");
      }
    } else {
      if (sessionStorage.getItem("adminSessionInfo")) {
        sessionInfo = JSON.parse(sessionStorage.getItem("adminSessionInfo"));
      } else {
        location.replace("login.html");
      }
    }
  } else {
    location.replace("login.html");
  }
  $(".toggle-password").click(function() {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
      input.attr("type", "text");
      document.getElementById("password-field1").innerHTML = "lock_open";
    } else {
      input.attr("type", "password");
      document.getElementById("password-field1").innerHTML = "lock_outline";
    }
  });
  $('#icon_prefix').on('focusout', function() {
    checkUserNameAvailablity();
  });
  $('#mobileNumber').on('focusout', function() {
    checkUserNumberAvailablity();
  });
  $('#email').on('focusout', function() {
    checkUserEmailAvailablity();
  });
});

function checkUserNumberAvailablity() {
  if (document.getElementById("mobileNumber").value != "") {
    var myObj = {};
    myObj["requestValue"] = document.getElementById("mobileNumber").value;
    myObj["requestType"] = "PHONE";
    myObj["requestFor"] = "ADM";
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
      url: beUrl() + 'uniquecreation/',
      type: 'POST',
      dataType: 'json',
      processData: false,
      contentType: 'application/json',
      data: jsonObj,
      success: function(response) {
        M.toast({
          html: 'Unique Phone number passed'
        });
      },
      error: function(response) {
        var usingInfo = JSON.parse(JSON.stringify(response));
        M.toast({
          html: usingInfo.responseJSON.Data
        });
      }
    });
  } else {
    M.toast({
      html: 'Phone number field cannot be left blank'
    });
  }
}

function checkUserEmailAvailablity() {
  if (document.getElementById("email").value != "") {
    var myObj = {};
    myObj["requestValue"] = document.getElementById("email").value;
    myObj["requestType"] = "EMAIL";
    myObj["requestFor"] = "ADM";
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
      url: beUrl() + 'uniquecreation/',
      type: 'POST',
      dataType: 'json',
      processData: false,
      contentType: 'application/json',
      data: jsonObj,
      success: function(response) {
        M.toast({
          html: 'Unique email id passed'
        });
      },
      error: function(response) {
        var usingInfo = JSON.parse(JSON.stringify(response));
        M.toast({
          html: usingInfo.responseJSON.Data
        });
      }
    });
  } else {
    M.toast({
      html: 'Email field cannot be left blank'
    });
  }
}

function checkUserNameAvailablity() {
  if (document.getElementById("icon_prefix").value != "") {
    var myObj = {};
    myObj["userName"] = document.getElementById("icon_prefix").value;
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
      url: beUrl() + 'uniqueusername/',
      type: 'POST',
      dataType: 'json',
      processData: false,
      contentType: 'application/json',
      data: jsonObj,
      success: function(response) {
        M.toast({
          html: 'Username available'
        });
      },
      error: function(response) {
        var usingInfo = JSON.parse(JSON.stringify(response));
        M.toast({
          html: usingInfo.responseJSON.Data
        });
      }
    });
  } else {
    M.toast({
      html: 'Username cannot be left blank'
    });
  }
}

function signUpUser() {
  event.preventDefault();
  var userName = document.getElementById("icon_prefix").value;
  var password = document.getElementById("password-field").value;
  var clientName = document.getElementById("clientName").value;
  var number = document.getElementById("mobileNumber").value;
  var email = document.getElementById("email").value;
  if (userName != "") {
    if (password != "") {
      if (clientName != "") {
        if (number != "") {
          if (email != "") {
            var myObj = {};
            myObj["userName"] = userName;
            myObj["password"] = password;
            myObj["adminName"] = clientName;
            myObj["number"] = number;
            myObj["email"] = email;
            myObj['userId'] = sessionInfo.sessionInfo.userId;
            myObj['typeId'] = sessionInfo.sessionInfo.typeId;
            myObj['userType'] = sessionInfo.sessionInfo.userType;
            var jsonObj = JSON.stringify(myObj);
            $.ajax({
              url: beUrl() + 'createadmin/',
              type: 'POST',
              dataType: 'json',
              processData: false,
              contentType: 'application/json',
              data: jsonObj,
              headers: {
                'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
                'sessionKey':sessionInfo.sessionInfo.sessionKey
              },
              success: function(response) {
                M.toast({
                  html: 'New admin user created'
                });
                location.assign("index.html");
              },
              error: function(response) {
                var usingInfo = JSON.parse(JSON.stringify(response));
                M.toast({
                  html: usingInfo.responseJSON.Data
                });
              }
            });
          } else {
            M.toast({
              html: 'Email cannot be left blank'
            });
          }
        } else {
          M.toast({
            html: 'Phone number cannot be left blank'
          });
        }
      } else {
        M.toast({
          html: 'Name cannot be left blank'
        });
      }
    } else {
      M.toast({
        html: 'Password cannot be left blank'
      });
    }
  } else {
    M.toast({
      html: 'Username cannot be left blank'
    });
  }
}
