function createBulkMovies() {
  event.preventDefault();
  var bulkJson = document.getElementById("bulkJson").value;
  if (bulkJson != "") {
    var myObj = {};
    myObj["requestObject"] = JSON.parse(bulkJson);
    myObj['userId'] = sessionInfo.sessionInfo.userId;
    myObj['typeId'] = sessionInfo.sessionInfo.typeId;
    myObj['userType'] = sessionInfo.sessionInfo.userType;
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
      url: beUrl() + 'createmoviebulk/',
      type: 'POST',
      dataType: 'json',
      processData: false,
      contentType: 'application/json',
      data: jsonObj,
      headers: {
        'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
        'sessionKey': sessionInfo.sessionInfo.sessionKey
      },
      success: function(response) {
        M.toast({
          html: 'New movie created'
        });
        location.assign("movies.html");
      },
      error: function(response) {
        var usingInfo = JSON.parse(JSON.stringify(response));
        M.toast({
          html: usingInfo.responseJSON.Data
        });
      }
    });
  }else {
    M.toast({
      html: 'Movie name cannot be left blank'
    });
  }
}
