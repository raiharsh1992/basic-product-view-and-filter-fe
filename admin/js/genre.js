var useData;
$(document).ready(function() {
  if(isDirect==0){
    console.log(sessionInfo);
  }
  displayProducts();
});

function displayProducts(){
  var myObj = {};
  myObj["viewType"]="ADM";
  myObj['userId'] = sessionInfo.sessionInfo.userId;
  myObj['typeId'] = sessionInfo.sessionInfo.typeId;
  myObj['userType'] = sessionInfo.sessionInfo.userType;
  var jsonObj = JSON.stringify(myObj);
  var url = beUrl() + 'viewgenre/';
  $.ajax({
    url: url,
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
      'sessionKey':sessionInfo.sessionInfo.sessionKey
    },
    success: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      var usingData = usingInfo.Data;
      useData = usingData;
      if (usingData.length > 0) {
        var lPrint = '';
        $.each(usingData, function(index, v) {
          if(v.isActive){
            lPrint = lPrint+"<tr class='hoverable'><td class='inLineText'>" + v.genreName + "</td><td><div class='switch'><label>Off<input type='checkbox' checked  onclick='changeStatusON(" + index + ")'><span class='lever'></span>On</label></div></td></tr>";
          }
          else{
            lPrint = lPrint+"<tr class='hoverable'><td class='inLineText'>" + v.genreName + "</td></td><td><div class='switch'><label>Off<input type='checkbox'  onclick='changeStatusOff(" + index + ")'><span class='lever'></span>On</label></div></td></tr>";
          }
        });

        document.getElementById("displayNothing").style.display = "none";
        document.getElementById("tableContainer").innerHTML = lPrint;
      } else {
        M.toast({
          html: 'No Genre to display please try again later!'
        });
        document.getElementById("tableContainer").innerHTML = "";
      }
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      console.log(usingInfo);
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });
}

function changeStatusON(index) {
  var usingCurrent = useData[index];
  var myObj = {};
  myObj["name"]=usingCurrent.genreName;
  myObj["genreId"] = usingCurrent.genreId;
  myObj["isActive"] = 0;
  myObj['userId'] = sessionInfo.sessionInfo.userId;
  myObj['typeId'] = sessionInfo.sessionInfo.typeId;
  myObj['userType'] = sessionInfo.sessionInfo.userType;
  var jsonObj = JSON.stringify(myObj);
  var url = beUrl() + 'editgenre/';
  $.ajax({
    url: url,
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
      'sessionKey':sessionInfo.sessionInfo.sessionKey
    },
    success: function(response) {
      M.toast({
        html: "Status updated successfully"
      });
      location.reload();
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      console.log(usingInfo);
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });
}

function changeStatusOff(index) {
  var usingCurrent = useData[index];
  var myObj = {};
  myObj["name"]=usingCurrent.genreName;
  myObj["genreId"] = usingCurrent.genreId;
  myObj["isActive"] = 1;
  myObj['userId'] = sessionInfo.sessionInfo.userId;
  myObj['typeId'] = sessionInfo.sessionInfo.typeId;
  myObj['userType'] = sessionInfo.sessionInfo.userType;
  var jsonObj = JSON.stringify(myObj);
  var url = beUrl() + 'editgenre/';
  $.ajax({
    url: url,
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
      'sessionKey':sessionInfo.sessionInfo.sessionKey
    },
    success: function(response) {
      M.toast({
        html: "Status updated successfully"
      });
      location.reload();
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      console.log(usingInfo);
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });
}
