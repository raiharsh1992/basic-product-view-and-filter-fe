function createGenreUser() {
  event.preventDefault();
  var genreName = document.getElementById("icon_prefix").value;
  if (genreName != "") {
    var myObj = {};
    myObj["name"] = genreName;
    myObj['userId'] = sessionInfo.sessionInfo.userId;
    myObj['typeId'] = sessionInfo.sessionInfo.typeId;
    myObj['userType'] = sessionInfo.sessionInfo.userType;
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
      url: beUrl() + 'creategenre/',
      type: 'POST',
      dataType: 'json',
      processData: false,
      contentType: 'application/json',
      data: jsonObj,
      headers: {
        'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
        'sessionKey': sessionInfo.sessionInfo.sessionKey
      },
      success: function(response) {
        M.toast({
          html: 'New genre created'
        });
        location.assign("genre.html");
      },
      error: function(response) {
        var usingInfo = JSON.parse(JSON.stringify(response));
        M.toast({
          html: usingInfo.responseJSON.Data
        });
      }
    });
  } else {
    M.toast({
      html: 'Genre name cannot be left blank'
    });
  }
}
