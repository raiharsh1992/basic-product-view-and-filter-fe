$(document).ready(function() {
  displayProducts();
});


function displayProducts() {
  var myObj = {};
  myObj["movieId"] = getUrlVars()["movieId"];
  var jsonObj = JSON.stringify(myObj);
  var url = beUrl() + 'moviedetails/';
  $.ajax({
    url: url,
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    success: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      var usingData = usingInfo.Data;
      document.getElementById("movieName").innerHTML = usingData.name;
      $("#director").append(usingData.director);
      $("#popularity").append(usingData.popularity);
      $("#imdb").append(usingData.imdb);
      var lPrint = ''
      $.each(usingData.genre, function(index, value) {
        if (index == 0) {
          lPrint = lPrint + value;
        } else {
          lPrint = lPrint + ", " + value;
        }
      })
      $("#genre").append(lPrint);
      lPrint = '';
      $.each(usingData.comments, function(index, value) {
        lPrint = lPrint + "<tr class='hoverable'><td>" + value.commentBy + "</td><td>" + value.comment + "</td></tr>"
      })
      $("#tableContainer").append(lPrint);
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });
}

function getUrlVars() {

  var vars = {};
  var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
    vars[key] = value;

  });
  return vars;
}
